import Browser
import Html exposing (Html, Attribute, div, input, text)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)


-- what's simplest model, view for our purposes?

-- one input field?

-- MAIN


main =
  Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
  { content  : String
  , result : String
  }


init : Model
init =
  { content = "1"
  , result = "" }



-- UPDATE


type Msg
  = Change String

resultToString : Result String Int -> String
resultToString r = case r of
                       Err e -> " foo " ++ e
                       Ok a  -> " bar " ++ String.fromInt a


update : Msg -> Model -> Model
update msg model = 
  case msg of
    Change newContent ->
      { model | content = newContent,
                result = resultToString (isReasonableAge newContent)  }



isReasonableAge : String -> Result String Int
isReasonableAge input = 
  case String.toInt input of
    Nothing -> 
      Err "That is not a number"

    Just age ->
      if age < 0 then
        Err "Please try again after you are born."

      else if age > 135 then
        Err "Are you a turtle?"

      else
        Ok age


-- VIEW


view : Model -> Html Msg
view model =
  div []
    [ input [ value model.content, onInput Change ] []
    , div [] [ text model.result ]
    ]
