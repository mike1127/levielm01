import Browser
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Array
import Random exposing (Generator, Seed, step)
import Debug


-- what's simplest model, view for our purposes?

-- one input field?

-- MAIN


main =
  Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
  { result      : List Int
  }

----------------------------------------------------------------------
--             ALGORITHMS

testList : List Int
testList = 
  [1, 2, 3, 4, 5, 6]


pickOdd : List Int -> List Int
pickOdd a = 
  List.filter (\x -> modBy 2 x /= 0) a

showNum x = 
  " " ++ String.fromInt x ++ ","


outputList = 
  pickOdd testList

init : Model
init =
  { result = outputList }



-- UPDATE


----------------------------------------------------------------------
-- Levi, ignore this part

type Msg
  = ButtonClick


resultToString : Result String Int -> String
resultToString r = case r of
                       Err e -> " foo " ++ e
                       Ok a  -> " bar " ++ String.fromInt a


update : Msg -> Model -> Model
update msg model = model


-- VIEW

numToDiv : Int -> Html Msg
numToDiv n =
  div [] [ text <| String.fromInt n ]


numToSpan : Int -> Html Msg
numToSpan n =
  span 

    [ style "margin-left" "5px"
    , style "margin-right" "5px"
    ] 

    [ text <| String.fromInt n ]


view : Model -> Html Msg
view model =
  div [] 
      [ div [] 
            [ text "foo" ]
      , input 

            [ type_ "button", value "a button"
            , onClick ButtonClick ] 

            []
      ]
