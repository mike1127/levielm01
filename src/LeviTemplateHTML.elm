import Browser
import Html exposing (Html, Attribute, div, input, text, span, button, img)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Array
import Random exposing (Generator, Seed, step)
import Debug


-- what's simplest model, view for our purposes?

-- one input field?

-- MAIN


main =
  Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
  { result      : List Int
  }


init = { result = [ 0, 1 ] }

----------------------------------------------------------------------
-- Levi, ignore this part

type Msg
  = ButtonClick


update : Msg -> Model -> Model
update msg model = model


-- VIEW

numToDiv : Int -> Html Msg
numToDiv n =
  div [] [ text <| String.fromInt n ]


numToSpan : Int -> Html Msg
numToSpan n =
  span 

    [ style "margin-left" "5px"
    , style "margin-right" "5px"
    ] 

    [ text <| String.fromInt n ]


createDiv : Maybe String -> Int -> Html Msg
createDiv flag n = 
  let alignSelf = 
        case flag of
            Nothing -> []
            Just s  -> [ style "align-self" s ]
  in div 
     ([ style "color" "red"
     , style "font-family" "sans-serif"
     , style "font-size" "100px"
     ] ++ alignSelf)
     [ text <| String.fromInt n ]
    

view : Model -> Html Msg
view model =
  div [] 
      [ div [ style "display" "flex"
            , style "flex-direction" "row"
            , style "align-items" "flex-start"
            , style "justify-content" "space-between" ] 
            [  img [src "test-img-01.png"] []
            , createDiv Nothing 1
            , createDiv (Just "center") 2 ]
      ]
