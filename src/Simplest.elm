import Browser
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Array
import Random exposing (Generator, Seed, step)
import Debug


-- what's simplest model, view for our purposes?

-- one input field?

-- MAIN


main =
  Browser.sandbox { init = init, update = update, view = view }


accumGen : Generator a -> (List a, Seed) -> (List a, Seed)
accumGen g ( xs, s ) = 
  let 
    ( value, s2 ) = step g s
  in 
    ( value :: xs, s2 )


accumGen3 : Seed -> Generator a -> (List a, Seed)
accumGen3 s g = accumGen g ( [], s ) |> accumGen g |> accumGen g

l3 g = [ accumGen g, accumGen g, accumGen g ]

nRandoms : Int -> Generator a -> Seed -> (List a, Seed)
nRandoms n g s = List.foldr (<|) ([], s) (List.repeat n <| accumGen g)


{-
nRandomInts : Random.Seed -> Int -> List Int
nRandomInts s n = 
  let 
      x : List (Generator Int)
      x = List.repeat 3 <| Random.int 0 10
      g1 = Random.int 0 10
      g2 = Random.int 0 12

  in
      Debug.todo "1253"
      -- List.foldl g Random.andThen Debug.todo "yup"
-}

-- MODEL


type alias Model =
  { result      : List Int
  , seed        : Random.Seed
  }


testList : List Int
testList = 
  [1, 2, 3, 4, 5, 6]


pickOdd : List Int -> List Int
pickOdd a = 
  List.filter (\x -> modBy 2 x /= 0) a

showNum x = 
  " " ++ String.fromInt x ++ ","


outputList = 
  pickOdd testList

init : Model
init =
  { result      = outputList
  , seed        = Random.initialSeed 0 }


randomFloat = 
  Random.float 0 1

randomInt =
  Random.int 0 10


-- UPDATE


type Msg
  = ButtonClick


resultToString : Result String Int -> String
resultToString r = case r of
                       Err e -> " foo " ++ e
                       Ok a  -> " bar " ++ String.fromInt a


update : Msg -> Model -> Model
update msg model = 
  case msg of
    ButtonClick ->
      let
          ( vs, newSeed ) = nRandoms 7 (Random.int 1 10) model.seed
      in
          { model | result = vs
          ,         seed = newSeed  }


-- VIEW

numToDiv : Int -> Html Msg
numToDiv n =
  div [] [ text <| String.fromInt n ]


numToSpan : Int -> Html Msg
numToSpan n =
  span 

    [ style "margin-left" "5px"
    , style "margin-right" "5px"
    ] 

    [ text <| String.fromInt n ]


view : Model -> Html Msg
view model =
  div [] 
      [ div [] 
            (List.map numToSpan model.result)
      , input 

            [ type_ "button", value "a button"
            , onClick ButtonClick ] 

            []
      ]
