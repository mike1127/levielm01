import Browser
import Html exposing (Html, Attribute, div, input, text)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)



-- MAIN


main =
  Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
  { input1  : String
  , result  : String
  }


init : Model
init =
  { input1 = ""
  , result = "" }



-- UPDATE


type Msg
  = Change String


update : Msg -> Model -> Model
update msg model = 
  case msg of
    Change newContent ->
      { model | input1 = newContent,
                result = computeResult newContent }


computeResult s = s

-- VIEW


view : Model -> Html Msg
view model =
  div []
    [ input [ value model.input1, onInput Change ] []
    , div [] [ text model.result ]
    ]
