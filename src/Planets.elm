import Html.Attributes exposing (..)
import Html exposing (Html, Attribute, div, input, text, span, button)
import Browser
import Browser.Events exposing (onAnimationFrameDelta)
import Canvas exposing (shapes, transform, translate, rotate, fill, rect)
import Color


type alias Point = 
  { x : Float 
  , y : Float }


type alias Planetoid =
  { mass : Float
  , pos  : Point
  , vel  : Point
  }


type alias Model =
  { planetoids : List Planetoid
  }


type Msg = Msg Float


main =
  Browser.element 
    { init = \() -> ( initModel, Cmd.none )
    , view = view
    , update = update
    , subscriptions = \model -> onAnimationFrameDelta Msg
    }


initModel : Model
initModel = { planetoids = [] }


clearScreen =
  shapes [fill Color.white] [rect (0, 0) 400 400]


view : Model -> Html Msg
view _ =
  div 
     [ ] 

     [ Canvas.toHtml
         ( 400, 400 )
         [ style "border" "10px solid rgba(0,0,0,0.1)" ]
         [ clearScreen
         , shapes [ fill (Color.hsl 170.0 0.5 0.5) ] 
                  [ rect (10, 10) 100 100 ]
         ]
     ]


update : Msg -> Model -> ( Model, Cmd Msg )
update _ m = ( m, Cmd.none )


