import Browser
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Array
import Random exposing (Generator, Seed, step)
import Debug


-- what's simplest model, view for our purposes?

-- one input field?

-- MAIN


main =
  Browser.sandbox { init = init, update = update, view = view }



-- MODEL


----------------------------------------------------------------------
----------------------------------------------------------------------
--                 FUNCTIONS TO EXPERIMENT WITH


type alias Model =
  { result      : List Int
  }


init : Model
init =
  { result = testList }


testList : List Int
testList = 
  [1, 2, 3, 4, 5, 6]


pickOdd : List Int -> List Int
pickOdd a = 
  List.filter (\x -> modBy 2 x /= 0) a


square : Int -> Int
square i = i * i


addPrime : Int -> List Int -> List Int
addPrime nToTest inputList = List.range 1 nToTest


outputList = 
  pickOdd testList



update : Msg -> Model -> Model
update msg model = 
  case msg of
    ButtonClick -> 
      { model | result = addPrime 3 model.result }



-- UPDATE


----------------------------------------------------------------------
-- Levi, ignore this part



type Msg
  = ButtonClick


-- VIEW

numToDiv : Int -> Html Msg
numToDiv n =
  div [] [ text <| String.fromInt n ]


numToSpan : Int -> Html Msg
numToSpan n =
  span 

    [ style "margin-left" "5px"
    , style "margin-right" "5px"
    ] 

    [ text <| String.fromInt n ]


view : Model -> Html Msg
view model =
  div [] 
      [ div [] 
            (List.map numToSpan model.result)
      , input 

            [ type_ "button", value "a button"
            , onClick ButtonClick ] 

            []
      ]
