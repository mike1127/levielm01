import Browser
import Browser.Events exposing (onAnimationFrameDelta)
import Html exposing (Html, Attribute, div, input, text, span, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Array
import Random exposing (Generator, Seed, step)
import Debug
import Canvas exposing (shapes, transform, translate, rotate, fill, rect)
import Color


-- what's simplest model, view for our purposes?

-- one input field?

-- MAIN



type alias Model = 
  { p : Float }

type Msg
  = Frame Float


main =
  Browser.element 
    { init = \() -> ( { t = 0 }, Cmd.none )
    , view = view
    , update = update
    , subscriptions = \model -> onAnimationFrameDelta Frame
    }


update : Msg -> Model -> ( Model, Cmd Msg)
update msg model = 
  case msg of
      Frame _ -> ( { model | t = model.t + 1 }, Cmd.none )


conf_width = 
  400


conf_height = 
  400


accumGen : Generator a -> (List a, Seed) -> (List a, Seed)
accumGen g ( xs, s ) = 
  let 
    ( value, s2 ) = step g s
  in 
    ( value :: xs, s2 )


accumGen3 : Seed -> Generator a -> (List a, Seed)
accumGen3 s g = accumGen g ( [], s ) |> accumGen g |> accumGen g

l3 g = [ accumGen g, accumGen g, accumGen g ]

nRandoms : Int -> Generator a -> Seed -> (List a, Seed)
nRandoms n g s = List.foldr (<|) ([], s) (List.repeat n <| accumGen g)


-- MODEL


init : Model
init =
  { t = 0.0 }


randomFloat = 
  Random.float 0 1

randomInt =
  Random.int 0 10


-- UPDATE

clearScreen =
  shapes [fill Color.white] [rect (0, 0) conf_width conf_height]


render count =
  let
    size = conf_width / 3
    x = -(size / 2)
    y = -(size / 2)
  in 
    shapes
      [ transform
          [ translate 200 200
          , rotate (degrees (count * 3))
          ]
      , fill (Color.hsl (degrees (count / 4)) 0.3 0.7)
      ]
      [ rect (x, y) size size ]


render2 count = shapes 
                [ fill (Color.hsl 20 0.0 0.0) ]
                [ rect (10, 10) 100 100 ]

-- VIEW

numToDiv : Int -> Html Msg
numToDiv n =
  div [] [ text <| String.fromInt n ]


numToSpan : Int -> Html Msg
numToSpan n =
  span 

    [ style "margin-left" "5px"
    , style "margin-right" "5px"
    ] 

    [ text <| String.fromInt n ]


view : Model -> Html Msg
view { t } =
  div 
     [ style "display" "flex"
     , style "justify-content" "center"
     , style "align-items" "center"
     ] 

     [ Canvas.toHtml
         ( conf_width, conf_height )
         [ style "border" "10px solid rgba(0,0,0,0.1)" ]
         [ clearScreen
         , render t
         ]
     ]
